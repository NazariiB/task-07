package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		var u = new User();
		u.setLogin(login);
		u.setId(0);
		return u;
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return login.compareTo(user.login) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, login);
	}
}
