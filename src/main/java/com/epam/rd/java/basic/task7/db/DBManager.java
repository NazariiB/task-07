package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.net.URI;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if(instance == null)
			instance = new DBManager();

		return instance;
	}

	private static Connection getConnection() throws DBException {
		FileReader fileReader = null;
		Connection con;
		try {
			fileReader = new FileReader("app.properties");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(fileReader);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String FULL_URL = prop.getProperty("connection.url");
		try {
			con =  DriverManager.getConnection(FULL_URL);
//			return DriverManager.getConnection(FULL_URL);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e.getCause());
		}
		return con;
	}

	private DBManager() {}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new LinkedList<>();
//		try (var con = DriverManager.getConnection(URL)){
		try(Connection con = getConnection()) {
			var stmt = con.createStatement();
			var res = stmt.executeQuery("select * from users");
			while (res.next()){
				User user = new User();
				user.setId(res.getInt("id"));
				user.setLogin(res.getString("login"));
				users.add(user);
				}
		}catch (SQLException e){
			users.add(User.createUser(e.getMessage()));
			e.printStackTrace();
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
//		try (var con = DriverManager.getConnection(URL)){
		try(Connection con = getConnection()) {
			con.createStatement().executeUpdate("INSERT INTO users VALUES (DEFAULT, '"+user.getLogin()+"')");
		}catch (SQLException e){
			e.printStackTrace();
		}
		return false;
	}



	public boolean deleteUsers(User... users) throws DBException {
		try (var con = getConnection()){
			for(var user:users){
				con.createStatement().executeUpdate("delete from users where login = '"+ user.getLogin() +"'");
			}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return false;
	}




	public User getUser(String login) throws DBException {
		User user = new User();
		try (var con = getConnection()){
			var stmt = con.createStatement();
			var res = stmt.executeQuery("select * from users where login='"+ login +"'");
			if(res.next()){
				user.setId(res.getInt("id"));
				user.setLogin(res.getString("login"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (var con = getConnection()){
			var stmt = con.createStatement();
			var res = stmt.executeQuery("select * from teams where name='"+ name +"'");
			if(res.next()){
				team.setId(res.getInt("id"));
				team.setName(res.getString("name"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new LinkedList<>();
		try (var con = getConnection()){
			var stmt = con.createStatement();
			var res = stmt.executeQuery("select * from teams");
			while (res.next()){
				Team team = new Team();
				 team.setId(res.getInt("id"));
				 team.setName(res.getString("name"));
				 teams.add(team);
			}
		}catch (SQLException e){
			teams.add(Team.createTeam(e.getMessage()));
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			var con = getConnection();
			con.createStatement().executeUpdate("INSERT INTO teams VALUES (DEFAULT, '"+ team.getName() +"')");
			con.close();
			team.setId(getTeam(team.getName()).getId());
		}catch (SQLException e){
			e.printStackTrace();
			return false;
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		try{
			int id = getUser(user.getLogin()).getId();
			List<Integer> ids = new LinkedList<>();
			for(var team:teams){
				ids.add(getTeam(team.getName()).getId());
			}
			con = getConnection();
			con.setAutoCommit(false);
			for(var team:teams) {
				con.createStatement().executeUpdate("insert into users_teams values(" + id + "," + ids.remove(0) +")");
			}
			con.commit();
		}catch (SQLException e){
			e.printStackTrace();
			try {
				con.rollback();
				con.close();
			}catch (SQLException e1){
				throw new DBException(e1.getMessage(), e1);
			}
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new LinkedList<>();
		int id = getUser(user.getLogin()).getId();
		try(var con = getConnection()) {
			var stmt = con.createStatement();
			var res = stmt.executeQuery("select user_id, team_id, name, id from users_teams left join teams on team_id=id");
		while (res.next()){
			if(res.getInt("user_id") == id) {
				Team team = new Team();
				team.setName(res.getString("name"));
				team.setId(res.getInt("id"));
				teams.add(team);
			}
		}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}



	public boolean deleteTeam(Team team) throws DBException {
		try(var con = getConnection()){
			con.createStatement().executeUpdate("delete from users_teams where team_id=" + team.getId());
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(var con = getConnection()) {
			con.createStatement().executeUpdate("update teams set name='"+ team.getName() +"' where id=" + team.getId());
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return false;
	}

}
